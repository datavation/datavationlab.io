This is another font that started life as a logo design project. The project was shelved and my intention was to finish the font as a standalone project, but other commitments have made this impossible. 

The font was originally going to contain only lower case characters, but I've mapped a set of characters to the upper case slots, which contains a few capital forms. 

Aside from full stop, comma, colon and semicolon, there are no punctuation marks or other symbols. Given its limitations, I'm making Cirqua available free for personal use with a commercial licence priced at just $2 (USD). Licences can be purchased by clicking the 'donate' button. 

All feedback is welcome and, if you desperately need any other characters added, contact steve@explogos.com and I'll see what I can do. 

Thanks!